Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OpenChemLib
Source: https://github.com/Actelion/openchemlib
Files-Excluded:
 .mvn
 mvnw
 mvnw.cmd
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MDHFeature.java
 src/main/java/info

Files: *
Copyright: 1997-2023, Idorsia Pharmaceuticals Ltd, Hegenheimermattweg 91, CH-4123 Allschwil, Switzerland
License: BSD-3-clause

Files: debian/*
Copyright: 2020-2023, Andrius Merkys <merkys@debian.org>
License: BSD-3-clause

Files: src/main/java/com/actelion/research/calc/statistics/median/MedianStatisticFunctions.java
 src/main/java/com/actelion/research/calc/statistics/median/ModelMedianFloat.java
 src/main/java/com/actelion/research/calc/statistics/median/ModelMedianInteger.java
 src/main/java/com/actelion/research/calc/statistics/median/ModelMedianLong.java
 src/main/java/com/actelion/research/chem/descriptor/DescriptorHandlerFlexophore.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/ClusterNode.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/DistHist.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/DistHistEncoder.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/ExceptionConformationGenerationFailed.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/IMolDistHist.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/IndexCoordinates.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MDHIndexTables.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MolDistHist.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MolDistHistEncoder.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MolDistHistViz.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MolDistHistVizEncoder.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/MolDistHistVizFrag.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/PPNode.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/PPNodeViz.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/PPNodeVizTriangle.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/UnparametrizedAtomTypeException.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/calculator/GeometryCalculator.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/calculator/IntQueue.java
 src/main/java/com/actelion/research/chem/descriptor/flexophore/calculator/StructureCalculator.java
 src/main/java/com/actelion/research/util/datamodel/IntegerDouble.java
 src/main/java/com/actelion/research/chem/AtomTypeList.java
 src/main/java/com/actelion/research/chem/Mutation.java
 src/main/java/com/actelion/research/chem/Mutator.java
 src/main/java/com/actelion/research/chem/ScaffoldHelper.java
 src/main/java/com/actelion/research/gui/CompoundCollectionListener.java
 src/main/java/com/actelion/research/gui/CompoundCollectionModel.java
 src/main/java/com/actelion/research/gui/CompoundCollectionPane.java
 src/main/java/com/actelion/research/gui/DefaultCompoundCollectionModel.java
 src/main/java/com/actelion/research/gui/JDrawDialog.java
 src/main/java/com/actelion/research/gui/JEditableStructureView.java
 src/main/java/com/actelion/research/gui/JStructureView.java
 src/main/java/com/actelion/research/gui/StructureListener.java
Copyright: 1997-2022, Idorsia Pharmaceuticals Ltd., Hegenheimermattweg 91, CH-4123 Allschwil, Switzerland
License: GPL-3+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  3. Neither the name of the the copyright holder nor the
     names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in /usr/share/common-licenses/GPL-3.
